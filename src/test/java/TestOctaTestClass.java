import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestOctaTestClass {
    WebDriver driver;

    @BeforeTest
    public void setup(){
        System.setProperty("webdriver.chrome.driver","Driver/chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test(priority=0)
    public void openMeetingURL() {
        driver.get("http://virtual.swecha.org/b/sru-hfv-gx4");
        // driver.get("https://demo.bigbluebutton.org/gl/raj-peh-xuc");
    }

    @Test(priority = 1)
    public void enterNameAndStartMeeting() {
        driver.findElement(By.xpath("//*[@id=\"_b_sru-hfv-gx4_join_name\"]")).sendKeys("RamGopal Verma");
        // driver.findElement(By.xpath("//*[@id=\"_gl_raj-peh-xuc_join_name\"]")).sendKeys("RamGopal Verma");
        driver.findElement(By.cssSelector("button.join-form")).click();
        // driver.findElement(By.xpath("/html/body/div[2]/div[1]/div/div[2]/div[2]/form/div/span/button")).click();
    }

    @Test(priority = 2)
    public void checkMicrophoneAndVideo(){
        driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/div/div/span/button[2]")).click();
        //driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/div/div/span/button[1]")).click();
        //driver.findElement(By.xpath("//*[@id=\"tippy-29\"]")).click();
    }

    @Test(priority = 3)
    public void enterText(){
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, 5000);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.ReactModal__Overlay")));
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"message-input\"]")));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"message-input\"]")));

        List<String> message = Arrays.asList(
                "hello",
                "How come burning start not burning corona virus",
                "Bio Ranjith should lead the biochemistry to solve carona"
        );
        message.forEach(this::sendText);
    }

    private void sendText(String text) {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[@id=\"message-input\"]")).sendKeys(text);
        driver.findElement(By.cssSelector("button.sendButton--Z93EzE")).click();
    }

   @AfterTest()
    public void closeDriver(){
        this.driver.close();
    }
}
